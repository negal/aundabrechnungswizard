package de.annathur.abrechnungswizard.model;

public class InputLastPage implements Page {
    private int lastStand = 0;
    private int lastNumber = 0;

    public int getLastStand() {
        return lastStand;
    }

    public void setLastStand(int lastStand) {
        this.lastStand = lastStand;
    }

    public int getLastNumber() {
        return lastNumber;
    }

    public void setLastNumber(int lastNumber) {
        this.lastNumber = lastNumber;
    }
}
