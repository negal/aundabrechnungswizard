package de.annathur.abrechnungswizard.model;

public class InputScheinCheckPage implements Page {
    private int remaining = 0;

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }
}
