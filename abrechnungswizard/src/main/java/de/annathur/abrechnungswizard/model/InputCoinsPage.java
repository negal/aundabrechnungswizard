package de.annathur.abrechnungswizard.model;

public class InputCoinsPage implements Page {
    private int value200 = 0;
    private int value100 = 0;
    private int value50 = 0;

    public int getValue200() {
        return value200;
    }

    public void setValue200(int value200) {
        this.value200 = value200;
    }

    public int getValue100() {
        return value100;
    }

    public void setValue100(int value100) {
        this.value100 = value100;
    }

    public int getValue50() {
        return value50;
    }

    public void setValue50(int value50) {
        this.value50 = value50;
    }
}
