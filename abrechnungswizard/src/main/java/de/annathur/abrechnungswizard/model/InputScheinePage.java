package de.annathur.abrechnungswizard.model;

public class InputScheinePage implements Page {
    private int value50 = 0;
    private int value20 = 0;
    private int value10 = 0;
    private int value5 = 0;

    public int getValue50() {
        return value50;
    }

    public void setValue50(int value50) {
        this.value50 = value50;
    }

    public int getValue20() {
        return value20;
    }

    public void setValue20(int value20) {
        this.value20 = value20;
    }

    public int getValue10() {
        return value10;
    }

    public void setValue10(int value10) {
        this.value10 = value10;
    }

    public int getValue5() {
        return value5;
    }

    public void setValue5(int value5) {
        this.value5 = value5;
    }
}
