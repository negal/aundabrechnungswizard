package de.annathur.abrechnungswizard.model;

public class ConditionText {
    public final String text;
    public final String condition;


    private ConditionText(String text, String condition) {
        this.text = text;
        this.condition = condition;
    }


    private ConditionText(String text) {
        this(text, null);
    }

    public static ConditionText conditionText(String text, String condition) {
        return new ConditionText(text, condition);
    }

    public static ConditionText conditionText(String text) {
        return new ConditionText(text);
    }
}
