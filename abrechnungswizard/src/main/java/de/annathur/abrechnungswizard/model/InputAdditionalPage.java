package de.annathur.abrechnungswizard.model;

import com.google.common.collect.Lists;

import java.util.List;

public class InputAdditionalPage implements Page {

    private final List<String> beschreibungen;
    private final List<Integer> betraege;

    public InputAdditionalPage() {
        beschreibungen = Lists.newArrayList("");
        betraege = Lists.newArrayList(0);
    }

    public void addRow() {
        beschreibungen.add("");
        betraege.add(0);
    }

    public void setBeschreibung(int i, String beschreibung) {
        beschreibungen.set(i, beschreibung);
    }

    public void setBetrag(int i, int betrag) {
        betraege.set(i, betrag);
    }

    public List<String> getBeschreibungen() {
        return beschreibungen;
    }

    public List<Integer> getBetraege() {
        return betraege;
    }
}
