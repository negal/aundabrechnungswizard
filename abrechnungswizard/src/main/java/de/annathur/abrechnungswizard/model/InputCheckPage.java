package de.annathur.abrechnungswizard.model;

public class InputCheckPage implements Page {
    private boolean[] checks;


    public InputCheckPage(int size) {
        checks = new boolean[size];
    }

    public void check(int i) {
        assert i < checks.length;
        assert i > 0;
        checks[i] = true;
    }

    public boolean[] getChecks() {
        return checks;
    }
}
