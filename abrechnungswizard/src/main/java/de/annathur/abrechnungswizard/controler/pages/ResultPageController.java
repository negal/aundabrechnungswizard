package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.view.pages.ResultPagePanel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static de.annathur.abrechnungswizard.controler.BetragUtil.centToOutput;

public class ResultPageController extends PageController<ResultPagePanel> {

    private final Logger logger = LoggerFactory.getLogger(ResultPageController.class);

    public ResultPageController(String description, int nr, int von) {
        super(description, nr, von);
        setView(new ResultPagePanel(getTitel(), isFirst(), isLast()));
    }

    @Override
    public void calculateResults(List<PageController> pages) {
        LocalDateTime datum = LocalDateTime.now();

        int lastStand = 0;
        int amountScheine = 0;
        int amountCoins = 0;
        int number = 0;
        int nichtInTresor = 0;

        StringBuilder zusaetzlich = new StringBuilder();
        int negativeSumme = 0;

        for (PageController page : pages) {
            if (page instanceof InputLastPageController) {
                InputLastPageController i = (InputLastPageController) page;
                lastStand = i.getLastStand();
                number = i.getLastNumber() + 1;
            } else if (page instanceof InputScheinePageController) {
                InputScheinePageController i = (InputScheinePageController) page;
                amountScheine = i.getSumme();
            } else if (page instanceof InputCoinsPageController) {
                InputCoinsPageController i = (InputCoinsPageController) page;
                amountCoins = i.getSumme();
            } else if (page instanceof InputAdditionalPageController) {
                InputAdditionalPageController i = (InputAdditionalPageController) page;

                List<String> beschreibungen = new ArrayList<>();
                List<Integer> betraege = new ArrayList<>();

                for (int j = 0; j < i.getModel().getBeschreibungen().size(); j++) {
                    if (StringUtils.isNotBlank(i.getModel().getBeschreibungen().get(j))) {
                        zusaetzlich.append(String.format("\n\t+ Ausgabe - %s: (-) %s €",
                                i.getModel().getBeschreibungen().get(j),
                                centToOutput(i.getModel().getBetraege().get(j))));
                        negativeSumme += i.getModel().getBetraege().get(j);
                        betraege.add(i.getModel().getBetraege().get(j));
                        beschreibungen.add(i.getModel().getBeschreibungen().get(j));
                    }
                }
                getView().setZusaetzlicheBetraege(betraege);
                getView().setZusaetzlicheTexte(beschreibungen);
            } else if (page instanceof InputScheinCheckPageController) {
                InputScheinCheckPageController i = (InputScheinCheckPageController) page;
                nichtInTresor = i.getModel().getRemaining() * 100;
            }
        }

        int inTresor = amountScheine - nichtInTresor;
        negativeSumme += inTresor;
        int kassenbestand = nichtInTresor + amountCoins; //positiveSumme - negativeSumme;
        int positiveSumme = negativeSumme + kassenbestand; //amountCoins + amountScheine;
        int tageseinnahmen = positiveSumme - lastStand;


        logger.info(String.format("Abrechnungsausgabe wurde generiert:" +
                        "\n\tDatum: %s" +
                        "\n\tNr: %s" +
                        "\n\tKassenbestand des Vortages: %s €" +
                        "\n\tTageseinnahmen: %s €" +
                        "%s" +
                        "\n\tIn den Tresor: (-) %s €" +
                        "\n\tSumme: %s €" +
                        "\n\t\tSumme (nur Scheine): %s €" +
                        "\n\t\tSumme (nur Muenzen): %s €" +
                        "\n\t- Ausgaben: %s €" +
                        "\n\tKassenbestand: %s €",
                datum.format(DATE_FORMATTER),
                String.valueOf(number),
                centToOutput(lastStand),
                centToOutput(tageseinnahmen),
                zusaetzlich.toString(),
                centToOutput(inTresor),
                centToOutput(positiveSumme),
                centToOutput(amountScheine),
                centToOutput(amountCoins),
                centToOutput(negativeSumme),
                centToOutput(kassenbestand)));

        getView().setDatum(datum.format(DATE_FORMATTER));
        getView().setNummer(String.valueOf(number));
        getView().setLetzterBestand(lastStand);
        getView().setTageseinnahmen(tageseinnahmen);
        getView().setPositiveSumme(positiveSumme);
        getView().setNegativeSumme(negativeSumme);
        getView().setKassenbestand(kassenbestand);
        getView().setInTresor(inTresor);
    }
}
