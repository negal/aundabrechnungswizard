package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.model.InputScheinePage;
import de.annathur.abrechnungswizard.view.pages.InputScheinePagePanel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class InputScheinePageController extends InputPageController<InputScheinePage, InputScheinePagePanel> {
    public InputScheinePageController(String description, String content, int nr, int von) {
        super(description, nr, von);
        setModel(new InputScheinePage());
        setView(new InputScheinePagePanel(getTitel(), content, isFirst(), isLast()));

        addChangeListeners();
    }

    @Override
    void readFromModel() {
        InputScheinePagePanel view = getView();
        InputScheinePage model = getModel();

        view.getText50er().setText(String.valueOf(model.getValue50()));
        view.getText50er().reset();
        view.getText20er().setText(String.valueOf(model.getValue20()));
        view.getText20er().reset();
        view.getText10er().setText(String.valueOf(model.getValue10()));
        view.getText10er().reset();
        view.getText5er().setText(String.valueOf(model.getValue5()));
        view.getText5er().reset();

        updateAmounts(view, model);
    }

    private void updateAmounts(InputScheinePagePanel view, InputScheinePage model) {
        view.getText50er().getAmountLabel().setValue(model.getValue50() * 5000);
        view.getText20er().getAmountLabel().setValue(model.getValue20() * 2000);
        view.getText10er().getAmountLabel().setValue(model.getValue10() * 1000);
        view.getText5er().getAmountLabel().setValue(model.getValue5() * 500);
        view.getTextSum().setValue(getSumme());
    }

    private void addChangeListeners() {
        InputScheinePagePanel view = getView();
        InputScheinePage model = getModel();
        view.getText50er().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setValue50(validateIntField(view.getText50er()));
                updateAmounts(view, model);
            }
        });
        view.getText20er().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setValue20(validateIntField(view.getText20er()));
                updateAmounts(view, model);
            }
        });
        view.getText10er().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setValue10(validateIntField(view.getText10er()));
                updateAmounts(view, model);
            }
        });
        view.getText5er().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setValue5(validateIntField(view.getText5er()));
                updateAmounts(view, model);
            }
        });
    }

    int getSumme() {
        return (getModel().getValue50() * 5000)
                + (getModel().getValue20() * 2000)
                + (getModel().getValue10() * 1000)
                + (getModel().getValue5() * 500);
    }
}
