package de.annathur.abrechnungswizard.controler;

import de.annathur.abrechnungswizard.importer.JSONImporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

public class Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
    private static final String FILE_PATH = "texte.json";

    public Controller() {
        SplashScreen splash = SplashScreen.getSplashScreen();
        LOGGER.info("Starte Controller");

        if (splash == null) {
            LOGGER.warn("Splash was not started");
        }

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            LOGGER.error("Window look and Feel was not found", e);
        }

        JSONImporter.load(FILE_PATH);
        if (JSONImporter.ERRORS.size() > 0) {
            boolean isInitialized = initErrorWindow();

            if (isInitialized) {
                JSONImporter.load(FILE_PATH);
                initMainWindow();
            }
        } else {
            initMainWindow();
        }

        if (splash != null) {
            splash.close();
        }
    }

    private void initMainWindow() {
        new MainWindowController();
    }

    private boolean initErrorWindow() {
        ErrorWindowController errorWindowController = new ErrorWindowController(FILE_PATH);
        return errorWindowController.isReset();
    }
}
