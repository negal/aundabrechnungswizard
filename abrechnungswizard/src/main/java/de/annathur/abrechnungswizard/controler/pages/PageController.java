package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.view.pages.PagePanel;

import java.time.format.DateTimeFormatter;
import java.util.List;

public abstract class PageController<V extends PagePanel> {

    static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private V view;
    private final String titel;
    private final boolean isFirst;
    private final boolean isLast;

    PageController(String description, int nr, int von) {
        titel = String.format("%d: %s", nr + 1, description);
        isFirst = nr == 0;
        isLast = (nr + 1) == von;
    }

    public V getView() {
        return view;
    }

    void setView(V view) {
        this.view = view;
    }

    public String getTitel() {
        return titel;
    }

    boolean isFirst() {
        return isFirst;
    }

    boolean isLast() {
        return isLast;
    }

    public abstract void calculateResults(List<PageController> pages);
}
