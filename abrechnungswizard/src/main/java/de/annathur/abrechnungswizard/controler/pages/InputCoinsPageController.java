package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.model.InputCoinsPage;
import de.annathur.abrechnungswizard.view.pages.InputCoinsPagePanel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class InputCoinsPageController extends InputPageController<InputCoinsPage, InputCoinsPagePanel> {

    public InputCoinsPageController(String description, String content, int nr, int von) {
        super(description, nr, von);
        setModel(new InputCoinsPage());
        setView(new InputCoinsPagePanel(getTitel(), content, isFirst(), isLast()));

        addChangeListeners();
    }

    @Override
    void readFromModel() {
        InputCoinsPagePanel view = getView();
        InputCoinsPage model = getModel();

        view.getText200er().setText(String.valueOf(model.getValue200()));
        view.getText200er().reset();

        view.getText100er().setText(String.valueOf(model.getValue100()));
        view.getText100er().reset();

        view.getText50er().setText(String.valueOf(model.getValue50()));
        view.getText50er().reset();

        updateAmounts(view, model);
    }

    private void updateAmounts(InputCoinsPagePanel view, InputCoinsPage model) {
        view.getText200er().getAmountLabel().setValue(model.getValue200() * 200);
        view.getText100er().getAmountLabel().setValue(model.getValue100() * 100);
        view.getText50er().getAmountLabel().setValue(model.getValue50() * 50);
        view.getTextSum().setValue(getSumme());
    }

    private void addChangeListeners() {
        InputCoinsPagePanel view = getView();
        InputCoinsPage model = getModel();

        view.getText200er().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setValue200(validateIntField(view.getText200er()));
                updateAmounts(view, model);

            }
        });

        view.getText100er().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setValue100(validateIntField(view.getText100er()));
                updateAmounts(view, model);

            }
        });

        view.getText50er().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setValue50(validateIntField(view.getText50er()));
                updateAmounts(view, model);
            }
        });
    }

    int getSumme() {
        return (getModel().getValue200() * 200)
                + (getModel().getValue100() * 100)
                + (getModel().getValue50() * 50);
    }
}
