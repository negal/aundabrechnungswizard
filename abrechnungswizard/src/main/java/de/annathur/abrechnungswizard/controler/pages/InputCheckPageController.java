package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.model.ConditionText;
import de.annathur.abrechnungswizard.model.InputCheckPage;
import de.annathur.abrechnungswizard.view.pages.InputChecklistPagePanel;

import java.util.List;

public class InputCheckPageController extends InputPageController<InputCheckPage, InputChecklistPagePanel> {

    public InputCheckPageController(String description, List<ConditionText> checkTexte, int nr, int von) {
        super(description, nr, von);
        setModel(new InputCheckPage(checkTexte.size()));
        setView(new InputChecklistPagePanel(getTitel(), checkTexte, isFirst(), isLast()));
    }

    @Override
    public void calculateResults(List<PageController> pages) {
    }

    @Override
    void readFromModel() {

    }
}
