package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.model.InputAdditionalPage;
import de.annathur.abrechnungswizard.view.pages.InputAdditionalPagePanel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static de.annathur.abrechnungswizard.controler.BetragUtil.centToOutput;

public class InputAdditionalPageController extends InputPageController<InputAdditionalPage, InputAdditionalPagePanel> {

    private int actualRow;

    public InputAdditionalPageController(String description, String content, int nr, int von) {
        super(description, nr, von);
        setModel(new InputAdditionalPage());
        setView(new InputAdditionalPagePanel(getTitel(), content, isFirst(), isLast()));

        actualRow = 0;

        addChangeListeners();
    }

    private void addChangeListeners() {
        InputAdditionalPagePanel view = getView();
        InputAdditionalPage model = getModel();

        if (view.getAddButton() != null) {
            view.getAddButton().addActionListener(e -> {
                actualRow++;
                view.addRow();
                addChangeListeners();
                model.addRow();
            });
        }

        for (int i = 0; i < actualRow + 1; i++) {
            final int finalI = i;
            view.getBeschreibungen().get(finalI).addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent e) {
                }

                @Override
                public void keyPressed(KeyEvent e) {
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    model.setBeschreibung(finalI, validateTextField(view.getBeschreibungen()
                            .get(finalI)));
                }
            });
            view.getBetraege().get(finalI).addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent e) {
                }

                @Override
                public void keyPressed(KeyEvent e) {
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    model.setBetrag(finalI, validateDecemField(view.getBetraege().get(finalI)));
                }
            });
        }
    }

    @Override
    void readFromModel() {
        InputAdditionalPagePanel view = getView();
        InputAdditionalPage model = getModel();

        for (int i = 0; i < actualRow + 1; i++) {
            view.getBetraege().get(i).setText(centToOutput(model.getBetraege().get(i)));
            view.getBetraege().get(i).reset();
        }
    }
}
