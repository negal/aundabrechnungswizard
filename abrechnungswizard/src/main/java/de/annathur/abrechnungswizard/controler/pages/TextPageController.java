package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.view.pages.TextPagePanel;

import java.util.List;

public class TextPageController extends PageController<TextPagePanel> {
    public TextPageController(String description, String text, int nr, int von) {
        super(description, nr, von);
        setView(new TextPagePanel(getTitel(), text, isFirst(), isLast()));
    }

    @Override
    public void calculateResults(List<PageController> pages) {
    }
}
