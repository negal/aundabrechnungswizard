package de.annathur.abrechnungswizard.controler;

import de.annathur.abrechnungswizard.importer.JSONImporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.*;

class ErrorWindowController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorWindowController.class);
    private static final String BACKUP_FILE = "/backup/texte_backup.json";
    private boolean isReset;

    public ErrorWindowController(String filePath) {
        isReset = false;
        File inputFile = new File(filePath);
        int n = initDialog(inputFile);

        if (n == 0) {
            copyFromBackup(inputFile);
        } else {
            LOGGER.info("Beende ohne Hauptfenster gestartet zu haben");
        }
    }

    private void copyFromBackup(File inputFile) {
        LOGGER.info("Erzeuge File neu");

        InputStream is = null;
        OutputStream os = null;

        try {
            is = this.getClass().getResourceAsStream(BACKUP_FILE);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);

            os = new FileOutputStream(inputFile);
            os.write(buffer);

            isReset = true;
        } catch (IOException e) {
            LOGGER.error("Fehler beim Kopieren der Files", e);
        } finally {
            try {
                assert is != null;
                is.close();
                assert os != null;
                os.close();
            } catch (IOException e) {
                LOGGER.error("Fehler beim Schließen der InputStreams", e);
            }
        }
    }

    private int initDialog(File inputFile) {
        StringBuilder builder = new StringBuilder("Folgende Fehler traten beim Einlesen der Inputdatei \n'")
                .append(inputFile.getAbsolutePath())
                .append("'\n auf:\n");
        JSONImporter.ERRORS.forEach(s -> builder.append("  - ").append(s).append("\n"));
        builder.append("Soll die Datei mit der Default-Datei überschrieben werden?");

        Object[] options = {"Ja",
                "Nein"};
        return JOptionPane.showOptionDialog(null,
                builder.toString(),
                "Einlesefehler",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);
    }

    public boolean isReset() {
        return isReset;
    }
}
