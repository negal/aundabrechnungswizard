package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.controler.BetragUtil;
import de.annathur.abrechnungswizard.model.Page;
import de.annathur.abrechnungswizard.view.ValidationTextField;
import de.annathur.abrechnungswizard.view.pages.PagePanel;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

abstract class InputPageController<M extends Page, V extends PagePanel> extends PageController<V> {
    private M model;

    private static final Pattern DECEM_PATTERN = Pattern.compile("^-?\\d+([.,]\\d\\d)?$");
    private static final Pattern INT_PATTERN = Pattern.compile("^\\d+$");
    private static final Pattern TEXT_PATTERN = Pattern.compile(".*\\S+.*");

    M getModel() {
        return model;
    }

    void setModel(M model) {
        this.model = model;
    }

    InputPageController(String description, int nr, int von) {
        super(description, nr, von);
    }

    String validateTextField(ValidationTextField textField) {
        return Optional.ofNullable(getValidatetText(textField, TEXT_PATTERN))
                .map(String::trim)
                .orElse("");
    }

    int validateDecemField(ValidationTextField textField) {
        return Optional.ofNullable(getValidatetText(textField, DECEM_PATTERN))
                .map(BetragUtil::inputToCent)
                .orElse(0);
    }

    int validateIntField(ValidationTextField textField) {
        return Optional.ofNullable(getValidatetText(textField, INT_PATTERN))
                .map(Integer::valueOf)
                .orElse(0);
    }

    private String getValidatetText(ValidationTextField textField, Pattern pattern) {
        String text = textField.getText().trim();
        Matcher matcher = pattern.matcher(text);
        if (!matcher.matches()) {
            textField.showFail();
            textField.repaint();
        } else {
            textField.reset();
            textField.repaint();
            return textField.getText();
        }
        return null;
    }

    @Override
    public void calculateResults(List<PageController> pages) {
        readFromModel();
    }

    abstract void readFromModel();
}
