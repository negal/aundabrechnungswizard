package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.model.ConditionText;
import de.annathur.abrechnungswizard.model.InputLastPage;
import de.annathur.abrechnungswizard.view.pages.InputLastPagePanel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import static de.annathur.abrechnungswizard.controler.BetragUtil.centToOutput;

public class InputLastPageController extends InputPageController<InputLastPage, InputLastPagePanel> {
    public InputLastPageController(String description, List<ConditionText> contents, int nr, int von) {
        super(description, nr, von);
        setModel(new InputLastPage());
        setView(new InputLastPagePanel(getTitel(), contents, isFirst(), isLast()));

        addChangeListeners();
    }

    @Override
    void readFromModel() {
        InputLastPagePanel view = getView();
        InputLastPage model = getModel();

        view.getLastStand().setText(centToOutput(model.getLastStand()));
        view.getLastStand().reset();

        view.getLastNumber().setText(String.valueOf(model.getLastNumber()));
        view.getLastNumber().reset();
    }

    private void addChangeListeners() {
        InputLastPagePanel view = getView();
        InputLastPage model = getModel();

        view.getLastStand().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setLastStand(validateDecemField(view.getLastStand()));

            }
        });

        view.getLastNumber().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                model.setLastNumber(validateIntField(view.getLastNumber()));
            }
        });
    }

    int getLastStand() {
        return getModel().getLastStand();
    }

    int getLastNumber() {
        return getModel().getLastNumber();
    }
}
