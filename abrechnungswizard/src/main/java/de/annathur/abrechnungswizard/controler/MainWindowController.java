package de.annathur.abrechnungswizard.controler;

import de.annathur.abrechnungswizard.controler.pages.PageController;
import de.annathur.abrechnungswizard.importer.JSONImporter;
import de.annathur.abrechnungswizard.view.MainWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;

class MainWindowController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainWindowController.class);

    private final List<PageController> pages;
    private final MainWindow window;
    private final JList<String> verlaufsListe;

    private int actualPageIndex;


    public MainWindowController() {
        actualPageIndex = 0;
        this.pages = JSONImporter.PAGES;
        this.verlaufsListe = initVerlaufsListe();
        this.window = initWindow();
    }

    private JList<String> initVerlaufsListe() {
        JList<String> verlaufsListe = new JList<>(pages.stream()
                .map(PageController::getTitel).toArray(String[]::new));
        verlaufsListe.setSelectedIndex(0);
        verlaufsListe.addListSelectionListener(e -> {
            if (e.getValueIsAdjusting()) {
                int newIndex = verlaufsListe.getSelectedIndex();
                if (newIndex > -1 && newIndex < pages.size()) {
                    changePageTo(newIndex);
                } else {
                    verlaufsListe.setSelectedIndex(actualPageIndex);
                    LOGGER.error("Ungueltiger Index {} reset to {}", newIndex, actualPageIndex);
                }
            }
        });
        return verlaufsListe;
    }

    private void changePageTo(int newIndex) {
        actualPageIndex = calcNewIndex(newIndex);
        verlaufsListe.setSelectedIndex(actualPageIndex);
        pages.get(actualPageIndex).calculateResults(pages);
        window.setContent(pages.get(actualPageIndex).getView());
        LOGGER.debug("New Page {}", actualPageIndex);
    }

    private int calcNewIndex(int newIndex) {
        if (newIndex < 0) return 0;
        if (newIndex >= pages.size()) return pages.size() - 1;
        return newIndex;
    }

    private MainWindow initWindow() {
        MainWindow window = new MainWindow();
        window.addWindowListener(new WindowListener() {

            @Override
            public void windowOpened(WindowEvent arg0) {
            }

            @Override
            public void windowIconified(WindowEvent arg0) {
            }

            @Override
            public void windowDeiconified(WindowEvent arg0) {
            }

            @Override
            public void windowDeactivated(WindowEvent arg0) {
            }

            @Override
            public void windowClosing(WindowEvent arg0) {
                safeAndClose();
            }

            @Override
            public void windowClosed(WindowEvent arg0) {
            }

            @Override
            public void windowActivated(WindowEvent arg0) {
            }
        });

        KeyboardFocusManager.getCurrentKeyboardFocusManager()
                .addKeyEventDispatcher(e -> {
                    if (e.getID() == KeyEvent.KEY_RELEASED && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
                        switch (e.getKeyCode()) {
                            case KeyEvent.VK_X:
                                changePageTo(actualPageIndex + 1);
                                break;
                            case KeyEvent.VK_Y:
                                changePageTo(actualPageIndex - 1);
                                break;
                            case KeyEvent.VK_1:
                                changePageTo(0);
                                break;
                            case KeyEvent.VK_2:
                                changePageTo(1);
                                break;
                            case KeyEvent.VK_3:
                                changePageTo(2);
                                break;
                            case KeyEvent.VK_4:
                                changePageTo(3);
                                break;
                            case KeyEvent.VK_5:
                                changePageTo(4);
                                break;
                            case KeyEvent.VK_6:
                                changePageTo(5);
                                break;
                            case KeyEvent.VK_7:
                                changePageTo(6);
                                break;
                            case KeyEvent.VK_8:
                                changePageTo(7);
                                break;
                            case KeyEvent.VK_9:
                                changePageTo(8);
                                break;
                            case KeyEvent.VK_0:
                                changePageTo(9);
                                break;
                        }

                    }
                    return false;
                });

        window.addVerlaufsListe(verlaufsListe);
        window.setContent(pages.get(actualPageIndex).getView());

        for (PageController page : pages) {
            page.getView().getNext().addActionListener(e -> changePageTo(actualPageIndex + 1));
            page.getView().getBack().addActionListener(e -> changePageTo(actualPageIndex - 1));
        }

        return window;
    }

    private void safeAndClose() {
        LOGGER.debug("Speichern...");
        LOGGER.info("Beende Controler");
    }
}
