package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.controler.BetragUtil;
import de.annathur.abrechnungswizard.model.ConditionText;
import de.annathur.abrechnungswizard.model.InputScheinCheckPage;
import de.annathur.abrechnungswizard.model.InputScheinePage;
import de.annathur.abrechnungswizard.view.pages.InputChecklistPagePanel;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputScheinCheckPageController extends InputPageController<InputScheinCheckPage, InputChecklistPagePanel> {

    private final static int MAX_SUMME = 20;

    public InputScheinCheckPageController(String description, List<ConditionText> contents, int nr, int von) {
        super(description, nr, von);
        setView(new InputChecklistPagePanel(getTitel(), contents, isFirst(), isLast()));
        setModel(new InputScheinCheckPage());
    }

    @Override
    public void calculateResults(List<PageController> pages) {

        InputScheinePage scheinModel = new InputScheinePage();

        for (PageController page : pages) {
            if (page instanceof InputScheinePageController) {
                InputScheinePageController i = (InputScheinePageController) page;
                scheinModel = i.getModel();
            }
        }
        InputScheinCheckPage model = this.getModel();

        int sum = scheinModel.getValue50() * 50 + scheinModel.getValue20() * 20 + scheinModel.getValue10() * 10 + scheinModel.getValue5() * 5;
        calcRemaining(scheinModel, model, sum);

        Map<String, String> parameters = new HashMap<>();
        parameters.put(":date:", LocalDateTime.now().format(DATE_FORMATTER));
        parameters.put(":remaining:", String.valueOf(getModel().getRemaining()));
        parameters.put(":toBank:", String.valueOf(sum - getModel().getRemaining()));

        getView().updateCheckBoxes(parameters);
    }

    private void calcRemaining(InputScheinePage scheinModel, InputScheinCheckPage model, int sum) {
        //Fall: Summe kann nicht gebildet werden, da weniger als MAX_SUMME in Kasse
        if (sum < MAX_SUMME) {
            model.setRemaining(sum);
        } else {
            int tempTarget = MAX_SUMME;

            //Fall: Wenn Summe, zwar groesser, aber aufgrund der Scheine kann die Summe nicht gebildet werden
            // -> nehme naechstgroessere
            while (!BetragUtil.doesSumMatch(scheinModel.getValue50(),
                    scheinModel.getValue20(),
                    scheinModel.getValue10(),
                    scheinModel.getValue5(), tempTarget)) {
                tempTarget += 5;
            }

            model.setRemaining(tempTarget);
        }
    }

    @Override
    void readFromModel() {
    }
}
