package de.annathur.abrechnungswizard.controler;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BetragUtil {

    public static String centToOutput(int betrag) {
        String[] output = centToOutputArray(betrag);
        return output[0] + "," + output[1];
    }

    public static int inputToCent(String input) {
        input = input.replaceAll("[^\\d.,-]", "");

        Pattern pattenWithPostfix = Pattern.compile("^(.*)[.,](\\d?)(\\d?)$");
        Matcher postFixmatcher = pattenWithPostfix.matcher(input);

        if (postFixmatcher.matches()) {
            String result = postFixmatcher.group(1).replaceAll("[.,]", "")
                    + (StringUtils.isNotBlank(postFixmatcher.group(2)) ? postFixmatcher.group(2) : "0")
                    + (StringUtils.isNotBlank(postFixmatcher.group(3)) ? postFixmatcher.group(3) : "0");
            return Integer.valueOf(result);
        }

        Pattern pattenWithoutPostfix = Pattern.compile("^-?([0-9.]*)$");
        Matcher matcher = pattenWithoutPostfix.matcher(input);

        if (matcher.matches()) {
            return Integer.valueOf(input.replaceAll("[.,]", "") + "00");
        }
        return 0;
    }

    public static String[] centToOutputArray(int betrag) {
        String[] result = new String[2];
        boolean positive = true;

        if (betrag < 0) {
            positive = false;
            betrag *= -1;
        }

        result[0] = (positive ? "" : "-") + "0";
        String betragsstring = String.valueOf(betrag);
        if (betrag < 10) {
            result[1] = "0" + betragsstring;
            return result;
        }
        if (betrag < 100) {
            result[1] = betragsstring;
            return result;
        }

        result[0] = (positive ? "" : "-") + betragsstring.substring(0, betragsstring.length() - 2);
        result[1] = betragsstring.substring(betragsstring.length() - 2);
        return result;
    }

    /**
     * Calculates if sum can be builded with existing scheines
     *
     * @return
     */
    public static boolean doesSumMatch(int in50er, int in20er, int in10er, int in5er, int targetSum) {

        while (targetSum > 0) {
            if (targetSum >= 50 && in50er > 0) {
                targetSum -= 50;
                in50er--;
            } else if (targetSum >= 20 && in20er > 0) {
                targetSum -= 20;
                in20er--;
            } else if (targetSum >= 10 && in10er > 0) {
                targetSum -= 10;
                in10er--;
            } else if (targetSum >= 5 && in5er > 0) {
                targetSum -= 5;
                in5er--;
            } else {
                return false;
            }
        }
        return true;
    }
}
