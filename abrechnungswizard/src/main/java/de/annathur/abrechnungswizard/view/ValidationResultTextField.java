package de.annathur.abrechnungswizard.view;

public class ValidationResultTextField extends ValidationTextField {

    private final AmountLabel amountLabel;

    public ValidationResultTextField() {
        super();

        amountLabel = new AmountLabel();
    }

    public AmountLabel getAmountLabel() {
        return amountLabel;
    }
}
