package de.annathur.abrechnungswizard.view;

import de.annathur.abrechnungswizard.importer.JSONImporter;

import javax.swing.*;
import java.awt.*;

public class ViewUtils {
    public static String htmlText(String text) {
        return String.format("<html>%s</html>", text);
    }

    public static String htmlTextWithLinkStyle(String text) {
        return String.format("<html><head>" +
                "<style type='text/css'>a{color:black;}</style>\n" +
                "  </head>%s</html>", text);
    }

    public static JLabel createTextLabel(String text) {
        JLabel textLabel = new JLabel(htmlText(text));
        textLabel.setFont(JSONImporter.FONT);
        return textLabel;
    }

    public static JEditorPane createHintText(String text) {
        JEditorPane jep = new JEditorPane();
        jep.setContentType("text/html");
        jep.setText(htmlTextWithLinkStyle(text));
        jep.setEditable(false);
        jep.setOpaque(false);
        jep.setBorder(BorderFactory.createEmptyBorder());
        jep.setBackground(new Color(0, 0, 0, 0));
        jep.setFont(JSONImporter.FONT);

        return jep;
    }
}
