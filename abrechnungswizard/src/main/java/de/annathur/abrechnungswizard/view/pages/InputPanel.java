package de.annathur.abrechnungswizard.view.pages;

import de.annathur.abrechnungswizard.model.ConditionText;
import de.annathur.abrechnungswizard.view.*;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import java.awt.*;

import static de.annathur.abrechnungswizard.importer.JSONImporter.EXPLANATIONS;
import static de.annathur.abrechnungswizard.view.ViewUtils.*;

abstract class InputPanel extends PagePanel {
    InputPanel(String header, boolean first, boolean last) {
        super(header, first, last);
    }

    GridBagConstraints initGridBagConstraints() {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);
        return gbc;
    }

    void addDescriptionRow(String text, GridBagConstraints gbc) {
        gbc.gridy = gbc.gridy + 1;

        gbc.ipadx = 0;
        gbc.ipady = 30;
        gbc.gridx = 0;
        gbc.gridwidth = 3;

        JEditorPane jep = createHintText(text);
        addHintField(jep, gbc);
    }

    private void addHintField(JEditorPane jep, GridBagConstraints gbc) {
        Panel panel = new Panel(new BorderLayout());

        panel.add(jep, BorderLayout.CENTER);
        this.getCenter().add(panel, gbc);

        gbc.gridy = gbc.gridy + 1;

        JLabel hintLabel = createTextLabel("");
        hintLabel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));


        JPanel hintPanel = new JPanel(new BorderLayout());
        hintPanel.setBorder(BorderFactory.createTitledBorder(""));
        hintPanel.setVisible(false);

        hintPanel.add(hintLabel, BorderLayout.CENTER);
        this.getCenter().add(hintPanel, gbc);

        jep.addHyperlinkListener(hle -> {
            if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
                hintLabel.setText(htmlText(EXPLANATIONS.get(hle.getDescription())));
                hintPanel.setVisible(!hintPanel.isVisible());
            }
        });
    }

    ValueCheckBox addCheckBoxRow(ConditionText text, GridBagConstraints gbc) {
        gbc.gridy = gbc.gridy + 1;

        gbc.ipadx = 0;
        gbc.ipady = 10;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        ValueCheckBox result = new ValueCheckBox(text);
        this.getCenter().add(result, gbc);

        gbc.gridx = 1;
        gbc.gridwidth = 2;
        this.getCenter().add(result.getTextLabel(), gbc);

        addHintField(result.getTextLabel(), gbc);

        return result;
    }

    ValidationTextField addTextfieldRow(String label, GridBagConstraints gbc) {
        gbc.gridwidth = 1;
        gbc.ipady = 0;

        gbc.gridy = gbc.gridy + 1;

        gbc.ipadx = 0;
        gbc.gridx = 0;
        this.getCenter().add(createTextLabel(label), gbc);

        gbc.gridx = 1;
        gbc.ipadx = 100;
        ValidationTextField result = new ValidationTextField();
        this.getCenter().add(result, gbc);

        return result;
    }

    ValidationResultTextField addResultTextfieldRow(String label, GridBagConstraints gbc) {
        gbc.gridwidth = 1;
        gbc.ipady = 0;

        gbc.gridy = gbc.gridy + 1;

        gbc.ipadx = 0;
        gbc.gridx = 0;
        this.getCenter().add(createTextLabel(label), gbc);

        gbc.gridx = 1;
        gbc.ipadx = 100;
        ValidationResultTextField result = new ValidationResultTextField();
        this.getCenter().add(result, gbc);

        gbc.gridx = 2;
        gbc.ipadx = 100;
        this.getCenter().add(result.getAmountLabel(), gbc);

        return result;
    }

    AmountLabel addSumRow(GridBagConstraints gbc) {
        gbc.gridy = gbc.gridy + 1;

        gbc.ipady = 30;

        gbc.ipadx = 0;
        gbc.gridx = 0;
        this.getCenter().add(createTextLabel("Summe"), gbc);

        gbc.gridx = 1;
        gbc.ipadx = 100;
        AmountLabel result = new AmountLabel();
        this.getCenter().add(result, gbc);

        return result;
    }

    SliderPanel addSliderRow(String label, double faktor, GridBagConstraints gbc) {
        gbc.gridy = gbc.gridy + 1;

        gbc.ipadx = 0;
        gbc.gridx = 0;
        this.getCenter().add(createTextLabel(label), gbc);

        gbc.gridx = 1;
        gbc.ipadx = 100;

        SliderPanel result = new SliderPanel(40, "%d €", faktor);
        this.getCenter().add(result, gbc);

        return result;
    }
}
