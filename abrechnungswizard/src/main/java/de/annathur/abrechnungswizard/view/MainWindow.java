package de.annathur.abrechnungswizard.view;

import de.annathur.abrechnungswizard.importer.JSONImporter;
import de.annathur.abrechnungswizard.view.pages.PagePanel;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    private final JPanel centerPanel;

    public MainWindow() {
        super();
        setLayout(new BorderLayout());

        centerPanel = new JPanel(new BorderLayout());
        centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        add(centerPanel, BorderLayout.CENTER);
        setSize(800, 600);
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle(JSONImporter.WINDOW_TITLE);
        setVisible(true);
    }

    public void addVerlaufsListe(JList<String> verlaufsListe) {
        add(new VerlaufsPanel(verlaufsListe), BorderLayout.WEST);
    }

    public void setContent(PagePanel pagePanel) {
        centerPanel.removeAll();
        centerPanel.add(pagePanel, BorderLayout.CENTER);

        this.revalidate();
        this.repaint();
    }
}
