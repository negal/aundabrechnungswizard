package de.annathur.abrechnungswizard.view.pages;

import de.annathur.abrechnungswizard.importer.JSONImporter;
import de.annathur.abrechnungswizard.model.ConditionText;
import de.annathur.abrechnungswizard.view.ValidationTextField;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class InputLastPagePanel extends InputPanel {

    private final ValidationTextField lastStand;
    private final ValidationTextField lastNumber;

    public InputLastPagePanel(String header, List<ConditionText> texts, boolean first, boolean last) {
        super(header, first, last);

        JPanel center = this.getCenter();

        GridBagLayout layout = new GridBagLayout();

        center.setLayout(layout);

        GridBagConstraints gbc = initGridBagConstraints();

        addDescriptionRow(texts.get(0).text, gbc);

        lastNumber = addTextfieldRow(JSONImporter.VOR_NR, gbc);
        lastStand = addTextfieldRow(JSONImporter.LAST_STAND, gbc);

        addDescriptionRow(texts.get(1).text, gbc);
    }

    public ValidationTextField getLastStand() {
        return lastStand;
    }

    public ValidationTextField getLastNumber() {
        return lastNumber;
    }
}
