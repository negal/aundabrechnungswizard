package de.annathur.abrechnungswizard.view.pages;

import de.annathur.abrechnungswizard.controler.BetragUtil;
import de.annathur.abrechnungswizard.importer.JSONImporter;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import static de.annathur.abrechnungswizard.controler.BetragUtil.centToOutputArray;

public class ResultPagePanel extends PagePanel {

    private static final String IMAGE = "/images/abrechnung_smal.jpg";

    private BufferedImage image;
    private String datum = "NOT_SET";
    private String nummer = "NOT_SET";
    private String[] letzterBestand = {"X", "XX"};
    private String[] tageseinnahmen = {"X", "XX"};
    private String[] positiveSumme = {"X", "XX"};
    private String[] negativeSumme = {"X", "XX"};
    private String[] kassenbestand = {"X", "XX"};
    private String[] inTresor = {"X", "XX"};
    private String[][] zusaetzlicheBetraege = {};
    private String[] zusaetzlicheTexte = {};


    public ResultPagePanel(String header, boolean first, boolean last) {
        super(header, first, last);
        JPanel center = this.getCenter();
        center.setLayout(new BorderLayout());

        try {
            image = ImageIO.read(getClass().getResource(IMAGE));
        } catch (IOException ex) {
            System.err.println("could not find image");
        }

        center.add(new AnzeigeDings(), BorderLayout.CENTER);
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public void setNummer(String nummer) {
        this.nummer = nummer;
    }

    public void setLetzterBestand(int letzterBestand) {
        this.letzterBestand = centToOutputArray(letzterBestand);
    }

    public void setTageseinnahmen(int tageseinnahmen) {
        this.tageseinnahmen = centToOutputArray(tageseinnahmen);
    }

    public void setPositiveSumme(int positiveSumme) {
        this.positiveSumme = centToOutputArray(positiveSumme);
    }

    public void setNegativeSumme(int negativeSumme) {
        this.negativeSumme = centToOutputArray(negativeSumme);
    }

    public void setKassenbestand(int kassenbestand) {
        this.kassenbestand = centToOutputArray(kassenbestand);
    }

    public void setInTresor(int inTresor) {
        this.inTresor = centToOutputArray(inTresor);
    }

    public void setZusaetzlicheBetraege(List<Integer> zusaetzlicheBetraege) {
        this.zusaetzlicheBetraege = zusaetzlicheBetraege.stream()
                .map(BetragUtil::centToOutputArray).toArray(String[][]::new);
    }

    public void setZusaetzlicheTexte(List<String> zusaetzlicheTexte) {
        this.zusaetzlicheTexte = zusaetzlicheTexte.toArray(new String[0]);
    }

    private class AnzeigeDings extends JPanel {

        private final int[] spaltenStartX = {41, 81, 277, 335, 361, 415, 439};
        private final int spaltenStartY = 63;
        private final float spaltenHoehe = 23.1666f;
        private final int padding = 4;
        private final int textBottom = 6;

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            double scaleHeigth = (double) this.getHeight() / image.getHeight();
            double scaleWidth = (double) this.getWidth() / image.getWidth();

            double scale = Math.min(scaleHeigth, scaleWidth);

            Graphics2D g2 = (Graphics2D) g;

            RenderingHints rh = new RenderingHints(
                    RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHints(rh);

            g2.scale(scale, scale);
            g2.drawImage(image, 0, 0, this);
            g2.setFont(new Font("Serif", Font.BOLD, 12));
            g2.drawString(datum, 420, 33);

            g2.setFont(new Font("Serif", Font.BOLD, 13));
            g2.drawString(nummer, 340, 33);

            drawBetrag(g2, letzterBestand, 3, 1);

            drawTextBetrag(g2, JSONImporter.TAGESEINNAHMEN, 1, 4);
            drawBetrag(g2, tageseinnahmen, 3, 4);

            drawBetrag(g2, positiveSumme, 3, 25);

            for (int i = 0; i < zusaetzlicheTexte.length; i++) {
                drawTextBetrag(g2, zusaetzlicheTexte[i], 1, 7 + i);
                drawBetrag(g2, zusaetzlicheBetraege[i], 5, 7 + i);
            }

            if (!StringUtils.equals(inTresor[0], "X") && !StringUtils.equals(inTresor[0], "0")) {
                drawTextBetrag(g2, "In den Tresor", 1, zusaetzlicheTexte.length + 9);
                drawBetrag(g2, inTresor, 5, zusaetzlicheTexte.length + 9);
            }

            drawBetrag(g2, negativeSumme, 5, 25);

            drawBetrag(g2, negativeSumme, 3, 26);
            drawBetrag(g2, kassenbestand, 3, 27);

            g2.setFont(new Font("Serif", Font.ITALIC, 15));
            g2.drawString(JSONImporter.UNTERSCHRIFT, 280, 720);
        }

        private void drawBetrag(Graphics2D g2, String[] betrag, int spalte, int zeile) {
            int y = spaltenStartY + Math.round(zeile * spaltenHoehe) - textBottom;

            g2.drawString(betrag[0], spaltenStartX[spalte] - (padding + g2.getFontMetrics().stringWidth(betrag[0])), y);
            g2.drawString(betrag[1], spaltenStartX[spalte] + 2, y);
        }

        private void drawTextBetrag(Graphics2D g2, String text, int spalte, int zeile) {
            int y = spaltenStartY + Math.round(zeile * spaltenHoehe) - textBottom;
            g2.drawString(text, spaltenStartX[spalte] + padding, y);
        }
    }
}
