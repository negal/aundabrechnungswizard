package de.annathur.abrechnungswizard.view.pages;

import de.annathur.abrechnungswizard.view.AmountLabel;
import de.annathur.abrechnungswizard.view.ValidationResultTextField;

import javax.swing.*;
import java.awt.*;

public class InputCoinsPagePanel extends InputPanel {

    private final ValidationResultTextField text200er;
    private final ValidationResultTextField text100er;
    private final ValidationResultTextField text50er;
    private final AmountLabel textSum;

    public InputCoinsPagePanel(String header, String text, boolean first, boolean last) {
        super(header, first, last);

        JPanel center = this.getCenter();

        GridBagLayout layout = new GridBagLayout();

        center.setLayout(layout);

        GridBagConstraints gbc = initGridBagConstraints();

        addDescriptionRow(text, gbc);

        text200er = addResultTextfieldRow("  2 €: ", gbc);
        text100er = addResultTextfieldRow("  1 €: ", gbc);
        text50er = addResultTextfieldRow("  50 ct: ", gbc);
        textSum = addSumRow(gbc);
    }

    public ValidationResultTextField getText200er() {
        return text200er;
    }

    public ValidationResultTextField getText100er() {
        return text100er;
    }

    public ValidationResultTextField getText50er() {
        return text50er;
    }

    public AmountLabel getTextSum() {
        return textSum;
    }
}
