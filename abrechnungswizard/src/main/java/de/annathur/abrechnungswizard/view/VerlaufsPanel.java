package de.annathur.abrechnungswizard.view;

import de.annathur.abrechnungswizard.importer.JSONImporter;

import javax.swing.*;
import java.awt.*;

class VerlaufsPanel extends JPanel {

    public VerlaufsPanel(JList<String> pageList) {
        super(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.add(new JScrollPane(pageList), BorderLayout.CENTER);
        pageList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pageList.setFont(JSONImporter.FONT);
    }
}
