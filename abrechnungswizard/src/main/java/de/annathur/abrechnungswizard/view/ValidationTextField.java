package de.annathur.abrechnungswizard.view;

import de.annathur.abrechnungswizard.importer.JSONImporter;

import javax.swing.*;
import javax.swing.border.AbstractBorder;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

public class ValidationTextField extends JTextField {

    public ValidationTextField() {
        super();

        this.setFont(JSONImporter.FONT);

        this.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                selectAll();
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });

        setOpaque(false);
        setBorder(new RoundedCornerBorder());
    }

    public void showFail() {
        ((RoundedCornerBorder) this.getBorder()).setWarning(true);
    }

    public void reset() {
        ((RoundedCornerBorder) this.getBorder()).setWarning(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (!isOpaque() && getBorder() instanceof RoundedCornerBorder) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setPaint(getBackground());
            g2.fill(((RoundedCornerBorder) getBorder()).getBorderShape(
                    0, 0, getWidth() - 1, getHeight() - 1));
            g2.dispose();
        }
        super.paintComponent(g);
    }

    class RoundedCornerBorder extends AbstractBorder {

        private Color aktColor = Color.GRAY;
        private int aktStrokeThickness = 1;

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            Shape border = getBorderShape(x + (aktStrokeThickness - 1), y + (aktStrokeThickness - 1),
                    width - (aktStrokeThickness + 2), height - (aktStrokeThickness + 2));
            g2.setPaint(UIManager.getColor("Panel.background"));
            Area corner = new Area(new Rectangle2D.Double(x, y, width, height));
            corner.subtract(new Area(border));
            g2.fill(corner);
            g2.setPaint(aktColor);
            g2.setStroke(new BasicStroke(aktStrokeThickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
            g2.draw(border);
            g2.dispose();
        }

        Shape getBorderShape(int x, int y, int w, int h) {
            int r = h; //h / 2;
            return new RoundRectangle2D.Double(x, y, w, h, r, r);
        }

        void setWarning(boolean isWarn) {
            this.aktColor = isWarn ? Color.red : Color.GRAY;
            this.aktStrokeThickness = isWarn ? 3 : 1;
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(4, 8, 4, 8);
        }

        @Override
        public Insets getBorderInsets(Component c, Insets insets) {
            insets.set(4, 8, 4, 8);
            return insets;
        }
    }
}
