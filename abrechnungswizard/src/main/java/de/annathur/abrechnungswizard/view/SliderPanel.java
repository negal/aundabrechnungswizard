package de.annathur.abrechnungswizard.view;

import de.annathur.abrechnungswizard.importer.JSONImporter;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.Hashtable;

public class SliderPanel extends JPanel {

    private static final String FORMAT = " = %.2f €";
    private final double faktor;
    private final JSlider slider;
    private final JLabel textLabel;

    public SliderPanel(int max, String format, double faktor) {
        super(new BorderLayout());
        this.faktor = faktor;

        slider = new JSlider(0, max, 0);
        slider.setMajorTickSpacing(10);
        slider.setMinorTickSpacing(1);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        Hashtable<Integer, JLabel> labelTable = new Hashtable<>();
        for (int i = 0; i < 50; i += 10) {
            labelTable.put(i, new JLabel(String.format(format, Math.round(faktor * i))));
        }
        slider.setLabelTable(labelTable);

        this.add(slider, BorderLayout.CENTER);

        textLabel = createTextLabel(String.format(FORMAT, (slider.getValue() * faktor)));
        this.add(textLabel, BorderLayout.EAST);
    }

    private JLabel createTextLabel(String text) {
        JLabel textLabel = new JLabel(text);
        textLabel.setFont(JSONImporter.FONT);
        return textLabel;
    }

    public void setValue(int value) {
        slider.setValue((int) Math.round(value / (faktor * 100)));
        textLabel.setText(String.format(FORMAT, (slider.getValue() * faktor)));
    }

    public int getValue() {
        textLabel.setText(String.format(FORMAT, (slider.getValue() * faktor)));
        return slider.getValue() * (int) (faktor * 100);
    }

    public void addSliderListener(ChangeListener l) {
        slider.addChangeListener(l);
    }
}
