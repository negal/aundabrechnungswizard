package de.annathur.abrechnungswizard.view;

import de.annathur.abrechnungswizard.importer.JSONImporter;

import javax.swing.*;

import static de.annathur.abrechnungswizard.controler.BetragUtil.centToOutput;
import static de.annathur.abrechnungswizard.view.ViewUtils.htmlText;

public class AmountLabel extends JLabel {

    public AmountLabel() {
        super(output(0));
        setFont(JSONImporter.FONT);
    }

    public void setValue(int amount) {
        setText(output(amount));
    }

    private static String output(int amount) {
        return htmlText(String.format("= %s €", centToOutput(amount)));
    }
}
