package de.annathur.abrechnungswizard.view.pages;

import javax.swing.*;
import java.awt.*;

import static de.annathur.abrechnungswizard.view.ViewUtils.createTextLabel;
import static de.annathur.abrechnungswizard.view.ViewUtils.htmlText;

public class TextPagePanel extends PagePanel {

    private final JLabel textLabel;

    public TextPagePanel(String header, String text, boolean first, boolean last) {
        super(header, first, last);

        textLabel = createTextLabel(text);

        this.getCenter().add(textLabel, BorderLayout.CENTER);
    }

    public void setText(String text) {
        textLabel.setText(htmlText(text));
    }
}
