package de.annathur.abrechnungswizard.view.pages;

import de.annathur.abrechnungswizard.importer.JSONImporter;

import javax.swing.*;
import java.awt.*;

public abstract class PagePanel extends JPanel {

    private final JPanel center;
    private final JButton back;
    private final JButton next;

    PagePanel(String header, boolean first, boolean last) {
        super(new BorderLayout());

        center = new JPanel();
        center.setBorder(BorderFactory.createTitledBorder(header));
        this.add(center, BorderLayout.CENTER);

        JPanel south = new JPanel(new BorderLayout());

        back = new JButton(JSONImporter.PREV);
        back.setFont(JSONImporter.FONT);
        next = new JButton(JSONImporter.NEXT);
        next.setFont(JSONImporter.FONT);

        if (!first) {
            south.add(back, BorderLayout.WEST);
        }
        if (!last) {
            south.add(next, BorderLayout.EAST);
        }

        this.add(south, BorderLayout.SOUTH);
    }

    JPanel getCenter() {
        return center;
    }

    public JButton getBack() {
        return back;
    }

    public JButton getNext() {
        return next;
    }
}
