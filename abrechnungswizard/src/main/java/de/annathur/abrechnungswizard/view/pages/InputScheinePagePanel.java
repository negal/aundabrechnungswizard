package de.annathur.abrechnungswizard.view.pages;

import de.annathur.abrechnungswizard.view.AmountLabel;
import de.annathur.abrechnungswizard.view.ValidationResultTextField;

import javax.swing.*;
import java.awt.*;

public class InputScheinePagePanel extends InputPanel {

    private final ValidationResultTextField text50er;
    private final ValidationResultTextField text20er;
    private final ValidationResultTextField text10er;
    private final ValidationResultTextField text5er;

    private final AmountLabel textSum;

    public InputScheinePagePanel(String header, String text, boolean first, boolean last) {
        super(header, first, last);

        JPanel center = this.getCenter();

        GridBagLayout layout = new GridBagLayout();

        center.setLayout(layout);

        GridBagConstraints gbc = initGridBagConstraints();

        addDescriptionRow(text, gbc);

        text50er = addResultTextfieldRow("50er: ", gbc);
        text20er = addResultTextfieldRow("20er: ", gbc);
        text10er = addResultTextfieldRow("10er: ", gbc);
        text5er = addResultTextfieldRow(" 5er: ", gbc);
        textSum = addSumRow(gbc);
    }

    public ValidationResultTextField getText50er() {
        return text50er;
    }

    public ValidationResultTextField getText20er() {
        return text20er;
    }

    public ValidationResultTextField getText10er() {
        return text10er;
    }

    public ValidationResultTextField getText5er() {
        return text5er;
    }

    public AmountLabel getTextSum() {
        return textSum;
    }
}
