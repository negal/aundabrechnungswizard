package de.annathur.abrechnungswizard.view.pages;

import com.google.common.collect.Lists;
import de.annathur.abrechnungswizard.importer.JSONImporter;
import de.annathur.abrechnungswizard.view.ValidationTextField;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static de.annathur.abrechnungswizard.view.ViewUtils.createTextLabel;

public class InputAdditionalPagePanel extends InputPanel {
    private final String text;
    private final List<ValidationTextField> beschreibungen;
    private final List<ValidationTextField> betraege;
    private JButton addButton;

    public InputAdditionalPagePanel(String header, String text, boolean first, boolean last) {
        super(header, first, last);
        this.text = text;
        beschreibungen = Lists.newArrayList(new ValidationTextField());
        betraege = Lists.newArrayList(new ValidationTextField());

        setComponents();
    }

    public void addRow() {
        beschreibungen.add(new ValidationTextField());
        betraege.add(new ValidationTextField());

        setComponents();
    }

    private void setComponents() {
        JPanel center = this.getCenter();

        center.removeAll();

        GridBagLayout layout = new GridBagLayout();

        center.setLayout(layout);

        GridBagConstraints gbc = initGridBagConstraints();

        addDescriptionRow(text, gbc);

        addBeschreibungsRow(JSONImporter.AUSGABEN, JSONImporter.BETRAEGE, gbc);

        for (int i = 0; i < beschreibungen.size(); i++) {
            addAdditionalTextfieldRow(beschreibungen.get(i), betraege.get(i), gbc);
        }

        if (beschreibungen.size() < 10) {
            addButton = addButtonRow("+", gbc);
        } else {
            addButton = null;
        }
        center.revalidate();
        center.repaint();
    }

    private JButton addButtonRow(String label, GridBagConstraints gbc) {
        gbc.ipadx = 100;
        gbc.gridy = gbc.gridy + 1;
        gbc.gridx = 0;
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton button = new JButton(label);
        button.setFont(JSONImporter.FONT);
        panel.add(button);
        this.getCenter().add(panel, gbc);
        return button;
    }

    private void addBeschreibungsRow(String text1, String text2, GridBagConstraints gbc) {
        gbc.gridwidth = 1;
        gbc.ipady = 0;

        gbc.ipadx = 100;
        gbc.gridy = gbc.gridy + 1;

        gbc.gridx = 0;
        this.getCenter().add(createTextLabel(text1), gbc);

        gbc.gridx = 1;
        this.getCenter().add(createTextLabel(text2), gbc);
    }

    private void addAdditionalTextfieldRow(ValidationTextField beschreibung, ValidationTextField betrag, GridBagConstraints gbc) {
        gbc.gridwidth = 1;
        gbc.ipady = 0;

        gbc.ipadx = 100;
        gbc.gridy = gbc.gridy + 1;

        gbc.gridx = 0;
        this.getCenter().add(beschreibung, gbc);

        gbc.gridx = 1;
        this.getCenter().add(betrag, gbc);
    }

    public List<ValidationTextField> getBeschreibungen() {
        return beschreibungen;
    }

    public List<ValidationTextField> getBetraege() {
        return betraege;
    }

    public JButton getAddButton() {
        return addButton;
    }
}
