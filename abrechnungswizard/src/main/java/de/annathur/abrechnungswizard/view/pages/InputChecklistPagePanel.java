package de.annathur.abrechnungswizard.view.pages;

import de.annathur.abrechnungswizard.model.ConditionText;
import de.annathur.abrechnungswizard.view.ValueCheckBox;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InputChecklistPagePanel extends InputPanel {

    private final List<ValueCheckBox> checkBoxes;

    public InputChecklistPagePanel(String header, List<ConditionText> checkTexte, boolean first, boolean last) {
        super(header, first, last);
        assert checkTexte.size() > 0;

        checkBoxes = new ArrayList<>();

        JPanel center = this.getCenter();

        GridBagLayout layout = new GridBagLayout();

        center.setLayout(layout);

        GridBagConstraints gbc = initGridBagConstraints();

        for (ConditionText text : checkTexte) {
            checkBoxes.add(addCheckBoxRow(text, gbc));
        }
    }

    public void updateCheckBoxes(Map<String, String> parameters) {
        for (ValueCheckBox checkBox : checkBoxes) {
            checkBox.updateValues(parameters);
        }
    }
}
