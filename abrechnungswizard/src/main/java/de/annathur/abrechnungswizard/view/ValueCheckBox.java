package de.annathur.abrechnungswizard.view;

import de.annathur.abrechnungswizard.model.ConditionText;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.util.Map;

import static de.annathur.abrechnungswizard.view.ViewUtils.createHintText;
import static de.annathur.abrechnungswizard.view.ViewUtils.htmlTextWithLinkStyle;

public class ValueCheckBox extends JCheckBox {
    private final ConditionText initialText;

    private final JEditorPane textLabel;

    static final ImageIcon OFF_BUTTON = new ImageIcon(ClassLoader.getSystemClassLoader().getResource("images/cb_off.png"));
    static final ImageIcon ON_BUTTON = new ImageIcon(ClassLoader.getSystemClassLoader().getResource("images/cb_on.png"));
    static final ImageIcon OFF_OVER_BUTTON = new ImageIcon(ClassLoader.getSystemClassLoader().getResource("images/cb_off_over.png"));
    static final ImageIcon ON_OVER_BUTTON = new ImageIcon(ClassLoader.getSystemClassLoader().getResource("images/cb_on_over.png"));

    public ValueCheckBox(ConditionText conditionText) {
        super(OFF_BUTTON);
        this.setSelectedIcon(ON_BUTTON);
        this.setIcon(OFF_BUTTON);
        this.setRolloverIcon(OFF_OVER_BUTTON);
        this.setRolloverSelectedIcon(ON_OVER_BUTTON);
        //TODO: Maybe cooler buttons?

        this.initialText = conditionText;
        this.textLabel = createHintText(conditionText.text);
    }

    public void updateValues(Map<String, String> paramters) {
        String temp = htmlTextWithLinkStyle(initialText.text);
        for (Map.Entry<String, String> p : paramters.entrySet()) {
            temp = temp.replaceAll(p.getKey(), p.getValue());
        }
        this.textLabel.setText(temp);

        this.setVisible(conditionIsNotActive() || paramHasContent(paramters.get(initialText.condition)));
        this.textLabel.setVisible(conditionIsNotActive() || paramHasContent(paramters.get(initialText.condition)));
    }

    public JEditorPane getTextLabel() {
        return textLabel;
    }

    private boolean conditionIsNotActive() {
        return initialText.condition == null;
    }

    private boolean paramHasContent(String s) {
        return StringUtils.isNotBlank(s) && !StringUtils.trim(s).equals("0");
    }
}
