package de.annathur.abrechnungswizard.importer;

public interface ImportConstants {
    String NOT_FOUND = "NOT FOUND";
    int TEXT_SIZE = 17;
    String VALIDATION_ERROR_MESSAGE = "Feld '%s' konnte nicht gefunden werden.";
    String VALIDATION_ERROR_NO_JSON = "Kein valides JSON in Datei '%s' gefunden.";
    String VALIDATION_ERROR_FILE_NOT_FOUND = "Datei '%s' existiert nicht.";
    String VALIDATION_ERROR_NO_PAGES = "Eingabedatei enthält keine Seiten ('pages')";

    String JSON_PAGES = "pages";
    String JSON_PAGE_TYPE = "type";
    String JSON_PAGE_DESCRIPTION = "description";
    String JSON_PAGE_CONTENT = "content";

    String JSON_CONTENT_TEXT = "text";

    String JSON_EXPLANATIONS = "explanations";
    String JSON_EXPLANATIONS_ID = "id";
    String JSON_EXPLANATIONS_DESCRIPTION = "description";
    String JSON_CONTENT_COND_PARAM = "conditionParam";
}
