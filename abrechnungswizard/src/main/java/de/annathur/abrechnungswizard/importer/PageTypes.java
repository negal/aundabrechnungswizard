package de.annathur.abrechnungswizard.importer;

public interface PageTypes {
    String TYPE_TEXT = "Text";
    String TYPE_SCHEINE = "InputScheine";
    String TYPE_SCHEIN_ACTION = "ScheinAction";
    String TYPE_CHECKS = "InputChecks";
    String TYPE_COIN = "InputCoins";
    String TYPE_LASTRESULT = "InputLast";
    String TYPE_ADD = "InputAdditional";
    String TYPE_RESULT = "Result";
}
