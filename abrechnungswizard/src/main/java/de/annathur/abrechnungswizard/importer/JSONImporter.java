package de.annathur.abrechnungswizard.importer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.annathur.abrechnungswizard.controler.pages.*;
import de.annathur.abrechnungswizard.model.ConditionText;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;

import static de.annathur.abrechnungswizard.importer.ImportConstants.*;
import static de.annathur.abrechnungswizard.importer.PageTypes.*;
import static de.annathur.abrechnungswizard.model.ConditionText.conditionText;

public final class JSONImporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JSONImporter.class);

    public static final List<String> ERRORS = new ArrayList<>();
    public static final List<PageController> PAGES = new ArrayList<>();

    public static final Map<String, String> EXPLANATIONS = new HashMap<>();

    public static String WINDOW_TITLE;
    public static String NEXT;
    public static String PREV;
    public static String AUSGABEN;
    public static String BETRAEGE;
    public static String VOR_NR;
    public static String LAST_STAND;
    public static String TAGESEINNAHMEN;
    public static String UNTERSCHRIFT;
    public static Font FONT;

    public static void load(final String filePath) {
        FONT = new Font("SansSerif", Font.PLAIN, TEXT_SIZE);

        initialFill();

        LOGGER.info("Importiere Texte aus '{}'", filePath);
        final ObjectMapper mapper = new ObjectMapper();
        File f = new File(filePath);
        LOGGER.info("Absolute Path: '{}'", f.getAbsolutePath());

        try {
            InputStream is = new FileInputStream(f);

            JsonNode jsonTree = mapper.readTree(is);

            if (jsonTree != null) {
                WINDOW_TITLE = readString(jsonTree, "windowTitle");
                NEXT = readString(jsonTree, "next");
                PREV = readString(jsonTree, "prev");
                AUSGABEN = readString(jsonTree, "ausgaben");
                BETRAEGE = readString(jsonTree, "betraege");
                VOR_NR = readString(jsonTree, "vorigeNr");
                LAST_STAND = readString(jsonTree, "lastStand");
                TAGESEINNAHMEN = readString(jsonTree, "tageseinnahmen");
                UNTERSCHRIFT = readString(jsonTree, "unterschrift");

                readPages(jsonTree);
                readExplanation(jsonTree);
            } else {
                addError(VALIDATION_ERROR_NO_JSON, f.getAbsolutePath());
            }
        } catch (FileNotFoundException e) {
            addError(VALIDATION_ERROR_FILE_NOT_FOUND, f.getAbsolutePath());
        } catch (IOException e) {
            addError(VALIDATION_ERROR_NO_JSON, f.getAbsolutePath());
        }
    }

    private static void addError(String errorMessage, String... params) {
        String message = String.format(errorMessage, (Object[]) params);
        ERRORS.add(message);
        LOGGER.error(message);
    }

    private static String readString(JsonNode jsonNode, String field) {
        return Optional.ofNullable(jsonNode.get(field))
                .map(JsonNode::asText)
                .orElseGet(() -> {
                    addError(VALIDATION_ERROR_MESSAGE, field);
                    return NOT_FOUND;
                });

    }

    private static void readPages(JsonNode jsonNode) {
        JsonNode pagesNode = jsonNode.get(JSON_PAGES);

        if (pagesNode == null) {
            addError(VALIDATION_ERROR_MESSAGE, JSON_PAGES);
        } else {

            Iterator<JsonNode> elements = pagesNode.elements();

            int pagesSize = pagesNode.size();

            if (pagesSize == 0) {
                addError(VALIDATION_ERROR_NO_PAGES);
            } else {
                PAGES.clear();
                int aktSize = 0;

                while (elements.hasNext()) {
                    JsonNode nextPage = elements.next();
                    String type = nextPage.get(JSON_PAGE_TYPE).asText();
                    String description = nextPage.get(JSON_PAGE_DESCRIPTION).asText();

                    String content = null;
                    List<ConditionText> contents = new ArrayList<>();
                    JsonNode contentNode = nextPage.get(JSON_PAGE_CONTENT);
                    if (contentNode.isTextual()) {
                        content = contentNode.asText();
                    } else if (contentNode.isArray()) {
                        Iterator<JsonNode> contentElements = contentNode.elements();
                        while (contentElements.hasNext()) {
                            JsonNode contentElement = contentElements.next();
                            if (contentElement.isTextual()) {
                                contents.add(conditionText(contentElement.asText()));
                            } else if (contentElement.isContainerNode()) {
                                String text = contentElement.get(JSON_CONTENT_TEXT).asText();
                                String condParam = contentElement.get(JSON_CONTENT_COND_PARAM).asText();
                                contents.add(conditionText(text, condParam));
                            }
                        }
                    } else {
                        addError(VALIDATION_ERROR_MESSAGE, JSON_PAGE_CONTENT);
                    }

                    if (StringUtils.equalsIgnoreCase(type, TYPE_TEXT)) {
                        PAGES.add(new TextPageController(description, content, aktSize, pagesSize));
                        aktSize++;
                    } else if (StringUtils.equalsIgnoreCase(type, TYPE_ADD)) {
                        PAGES.add(new InputAdditionalPageController(description, content, aktSize, pagesSize));
                        aktSize++;
                    } else if (StringUtils.equalsIgnoreCase(type, TYPE_COIN)) {
                        PAGES.add(new InputCoinsPageController(description, content, aktSize, pagesSize));
                        aktSize++;
                    } else if (StringUtils.equalsIgnoreCase(type, TYPE_LASTRESULT)) {
                        PAGES.add(new InputLastPageController(description, contents, aktSize, pagesSize));
                        aktSize++;
                    } else if (StringUtils.equalsIgnoreCase(type, TYPE_RESULT)) {
                        PAGES.add(new ResultPageController(description, aktSize, pagesSize));
                        aktSize++;
                    } else if (StringUtils.equalsIgnoreCase(type, TYPE_SCHEINE)) {
                        PAGES.add(new InputScheinePageController(description, content, aktSize, pagesSize));
                        aktSize++;
                    } else if (StringUtils.equalsIgnoreCase(type, TYPE_SCHEIN_ACTION)) {
                        PAGES.add(new InputScheinCheckPageController(description, contents, aktSize, pagesSize));
                        aktSize++;
                    } else if (StringUtils.equalsIgnoreCase(type, TYPE_CHECKS)) {
                        PAGES.add(new InputCheckPageController(description, contents, aktSize, pagesSize));
                        aktSize++;
                    }
                }
            }
        }
    }

    private static void readExplanation(JsonNode jsonNode) {
        JsonNode pagesNode = jsonNode.get(JSON_EXPLANATIONS);

        if (pagesNode == null) {
            addError(VALIDATION_ERROR_MESSAGE, JSON_EXPLANATIONS);
        } else {

            Iterator<JsonNode> elements = pagesNode.elements();

            EXPLANATIONS.clear();

            while (elements.hasNext()) {
                JsonNode nextExplanation = elements.next();
                String id = nextExplanation.get(JSON_EXPLANATIONS_ID).asText();
                String description = nextExplanation.get(JSON_EXPLANATIONS_DESCRIPTION).asText();

                EXPLANATIONS.put(id, description);
            }
        }
    }

    private static void initialFill() {
        PAGES.clear();
        EXPLANATIONS.clear();
        ERRORS.clear();

        PAGES.add(new TextPageController(NOT_FOUND, NOT_FOUND, 0, 0));

        WINDOW_TITLE = NOT_FOUND;
        NEXT = NOT_FOUND;
        PREV = NOT_FOUND;
        AUSGABEN = NOT_FOUND;
        BETRAEGE = NOT_FOUND;
        VOR_NR = NOT_FOUND;
        LAST_STAND = NOT_FOUND;
        TAGESEINNAHMEN = NOT_FOUND;
        UNTERSCHRIFT = NOT_FOUND;
    }
}
