package de.annathur.abrechnungswizard;

import de.annathur.abrechnungswizard.controler.Controller;

public class Starter {
    public static void main(String... args) {
        new Controller();
    }
}
