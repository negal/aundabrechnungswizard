package de.annathur.abrechnungswizard.view;

import junit.framework.TestCase;
import org.junit.Ignore;

import java.util.HashMap;
import java.util.Map;

import static de.annathur.abrechnungswizard.model.ConditionText.conditionText;

@Ignore
public class ValueCheckBoxTest extends TestCase {

    public void testUpdateValues() {
        ValueCheckBox testInstance = new ValueCheckBox(conditionText("A B :c: D :e:", ":c:"));

        assertEquals("<html>A B :c: D :e:</html>", testInstance.getText());
        assertTrue(testInstance.isVisible());

        Map<String, String> params = new HashMap<>();
        testInstance.updateValues(params);

        assertEquals("<html>A B :c: D :e:</html>", testInstance.getText());
        assertFalse(testInstance.isVisible());

        params.put(":c:", "C");
        testInstance.updateValues(params);

        assertEquals("<html>A B C D :e:</html>", testInstance.getText());
        assertTrue(testInstance.isVisible());

        params.put(":c:", "C1");
        params.put(":e:", "E");
        testInstance.updateValues(params);

        assertEquals("<html>A B C1 D E</html>", testInstance.getText());
        assertTrue(testInstance.isVisible());

        params.put(":c:", "0");
        testInstance.updateValues(params);

        assertEquals("<html>A B 0 D E</html>", testInstance.getText());
        assertFalse(testInstance.isVisible());

        params.put(":c:", "100");
        testInstance.updateValues(params);

        assertEquals("<html>A B 100 D E</html>", testInstance.getText());
        assertTrue(testInstance.isVisible());
    }

    public void testUpdateValuesWithNoCondition() {
        ValueCheckBox testInstance = new ValueCheckBox(conditionText("A"));
        assertTrue(testInstance.isVisible());

        Map<String, String> params = new HashMap<>();
        testInstance.updateValues(params);
        assertTrue(testInstance.isVisible());

        params.put(":c:", "C");
        testInstance.updateValues(params);
        assertTrue(testInstance.isVisible());
    }
}