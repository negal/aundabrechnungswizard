package de.annathur.abrechnungswizard.controler.pages;

import com.google.common.collect.Lists;
import de.annathur.abrechnungswizard.model.InputScheinCheckPage;
import de.annathur.abrechnungswizard.model.InputScheinePage;
import org.junit.Test;

import static de.annathur.abrechnungswizard.model.ConditionText.conditionText;
import static org.junit.Assert.assertEquals;

public class InputScheinCheckPageControllerTest {

    @Test
    public void calculateResults() {
        InputScheinCheckPageController scheinActionPageControler = new InputScheinCheckPageController("", Lists.newArrayList(conditionText("A")), 1, 1);
        InputScheinCheckPage model = scheinActionPageControler.getModel();

        scheinActionPageControler.calculateResults(Lists.newArrayList());

        assertEquals(0, model.getRemaining());

        InputScheinePageController inputScheinePageControler = new InputScheinePageController("", "", 1, 1);
        InputScheinePage scheineModel = inputScheinePageControler.getModel();
        scheineModel.setValue5(1);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(5, model.getRemaining());

        scheineModel.setValue5(3);
        scheineModel.setValue10(1);
        scheineModel.setValue20(1);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(20, model.getRemaining());


        scheineModel.setValue5(4);
        scheineModel.setValue10(1);
        scheineModel.setValue20(1);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(20, model.getRemaining());

        scheineModel.setValue5(0);
        scheineModel.setValue10(0);
        scheineModel.setValue20(0);
        scheineModel.setValue50(1);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(50, model.getRemaining());

        scheineModel.setValue5(4);
        scheineModel.setValue10(3);
        scheineModel.setValue20(2);
        scheineModel.setValue50(1);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(20, model.getRemaining());

        scheineModel.setValue5(1);
        scheineModel.setValue10(0);
        scheineModel.setValue20(3);
        scheineModel.setValue50(0);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(20, model.getRemaining());

        scheineModel.setValue5(0);
        scheineModel.setValue10(0);
        scheineModel.setValue20(9);
        scheineModel.setValue50(0);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(20, model.getRemaining());

        scheineModel.setValue5(1);
        scheineModel.setValue10(1);
        scheineModel.setValue20(0);
        scheineModel.setValue50(1);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(50, model.getRemaining());

        scheineModel.setValue5(1);
        scheineModel.setValue10(1);
        scheineModel.setValue20(0);
        scheineModel.setValue50(0);

        scheinActionPageControler.calculateResults(Lists.newArrayList(inputScheinePageControler));

        assertEquals(15, model.getRemaining());
    }
}