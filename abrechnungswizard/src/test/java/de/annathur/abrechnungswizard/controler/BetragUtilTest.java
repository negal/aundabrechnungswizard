package de.annathur.abrechnungswizard.controler;

import org.junit.Test;

import static org.junit.Assert.*;

public class BetragUtilTest {

    @Test
    public void centToOutput() {
        assertEquals("0,00", BetragUtil.centToOutput(0));

        assertEquals("0,01", BetragUtil.centToOutput(1));
        assertEquals("0,25", BetragUtil.centToOutput(25));
        assertEquals("123456,78", BetragUtil.centToOutput(12345678));

        assertEquals("-0,01", BetragUtil.centToOutput(-1));
        assertEquals("-0,25", BetragUtil.centToOutput(-25));
        assertEquals("-123456,78", BetragUtil.centToOutput(-12345678));
    }

    @Test
    public void centToOutputArray() {
        assertEquals("0", BetragUtil.centToOutputArray(0)[0]);
        assertEquals("00", BetragUtil.centToOutputArray(0)[1]);

        assertEquals("0", BetragUtil.centToOutputArray(1)[0]);
        assertEquals("01", BetragUtil.centToOutputArray(1)[1]);
        assertEquals("0", BetragUtil.centToOutputArray(25)[0]);
        assertEquals("25", BetragUtil.centToOutputArray(25)[1]);
        assertEquals("123456", BetragUtil.centToOutputArray(12345678)[0]);
        assertEquals("78", BetragUtil.centToOutputArray(12345678)[1]);

        assertEquals("-0", BetragUtil.centToOutputArray(-1)[0]);
        assertEquals("01", BetragUtil.centToOutputArray(-1)[1]);
        assertEquals("-0", BetragUtil.centToOutputArray(-25)[0]);
        assertEquals("25", BetragUtil.centToOutputArray(-25)[1]);
        assertEquals("-123456", BetragUtil.centToOutputArray(-12345678)[0]);
        assertEquals("78", BetragUtil.centToOutputArray(-12345678)[1]);
    }

    @Test
    public void inputToCent() {
        assertEquals(0, BetragUtil.inputToCent("0,00"));

        assertEquals(1, BetragUtil.inputToCent("0,01"));
        assertEquals(25, BetragUtil.inputToCent("0,25"));
        assertEquals(12345678, BetragUtil.inputToCent("123456,78"));

        assertEquals(-1, BetragUtil.inputToCent("-0,01"));
        assertEquals(-25, BetragUtil.inputToCent("-0,25"));
        assertEquals(-12345678, BetragUtil.inputToCent("-123456,78"));

        assertEquals(-12345678, BetragUtil.inputToCent("-123456.78"));
        assertEquals(12345678, BetragUtil.inputToCent("123456.78"));

        assertEquals(-12345678, BetragUtil.inputToCent("-123456,78  "));
        assertEquals(12345678, BetragUtil.inputToCent(" 123456.78€"));
        assertEquals(45678000, BetragUtil.inputToCent("456.780 €"));

        assertEquals(67800, BetragUtil.inputToCent("678"));
        assertEquals(1200, BetragUtil.inputToCent(" 12€"));
    }


    @Test
    public void doesSumMatch() {

        assertTrue(BetragUtil.doesSumMatch(0, 0, 0, 0, 0));
        assertFalse(BetragUtil.doesSumMatch(0, 0, 0, 0, 5));
        assertTrue(BetragUtil.doesSumMatch(10, 10, 10, 10, 5));
        assertFalse(BetragUtil.doesSumMatch(1, 0, 0, 0, 5));
        assertTrue(BetragUtil.doesSumMatch(1, 0, 0, 1, 5));
        assertFalse(BetragUtil.doesSumMatch(1, 0, 0, 1, 20));
        assertFalse(BetragUtil.doesSumMatch(1, 0, 1, 1, 20));
        assertTrue(BetragUtil.doesSumMatch(1, 0, 1, 2, 20));
        assertFalse(BetragUtil.doesSumMatch(0, 3, 0, 0, 50));
    }
}