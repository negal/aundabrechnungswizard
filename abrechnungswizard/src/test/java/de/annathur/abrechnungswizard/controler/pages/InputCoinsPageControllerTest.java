package de.annathur.abrechnungswizard.controler.pages;

import de.annathur.abrechnungswizard.model.InputCoinsPage;
import de.annathur.abrechnungswizard.view.pages.InputCoinsPagePanel;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InputCoinsPageControllerTest {

    private InputCoinsPageController inputCoinsPageController;

    @Before
    public void setUp() {
        inputCoinsPageController = new InputCoinsPageController("inputCoins", "AAAAAA", 1, 1);
    }

    @Test
    public void getSumme() {
        InputCoinsPage model = inputCoinsPageController.getModel();

        assertEquals(0, inputCoinsPageController.getSumme());

        model.setValue200(22);
        model.setValue100(31);
        model.setValue50(7);

        assertEquals(7850, inputCoinsPageController.getSumme());
    }
}