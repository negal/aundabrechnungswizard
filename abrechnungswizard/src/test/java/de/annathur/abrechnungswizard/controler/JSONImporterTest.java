package de.annathur.abrechnungswizard.controler;

import de.annathur.abrechnungswizard.controler.pages.*;
import de.annathur.abrechnungswizard.importer.JSONImporter;
import org.junit.Ignore;
import org.junit.Test;

import java.awt.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@Ignore
public class JSONImporterTest {
    private static final String NOT_EXISTING_FILE_PATH = "xxx.json";
    private static final String EMPTY_FILE_PATH = JSONImporterTest.class.getClassLoader().getResource("empty_file.json").getPath();
    private static final String EMPTY_TEXTE_PATH = JSONImporterTest.class.getClassLoader().getResource("empty_texte.json").getPath();
    private static final String EMPTY_CONTENT_PATH = JSONImporterTest.class.getClassLoader().getResource("empty_content.json").getPath();
    private static final String CONTENT_PATH = JSONImporterTest.class.getClassLoader().getResource("content.json").getPath();
    private static final String NOT_FOUND = "NOT FOUND";

    @Test
    public void loadNotExisting() {
        JSONImporter.load(NOT_EXISTING_FILE_PATH);

        assertThat(JSONImporter.AUSGABEN, is(NOT_FOUND));
        assertThat(JSONImporter.BETRAEGE, is(NOT_FOUND));
        assertThat(JSONImporter.FONT, isA(Font.class));
        assertThat(JSONImporter.FONT, notNullValue());
        assertThat(JSONImporter.LAST_STAND, is(NOT_FOUND));
        assertThat(JSONImporter.NEXT, is(NOT_FOUND));
        assertThat(JSONImporter.PAGES, notNullValue());
        assertThat(JSONImporter.PAGES.size(), is(1));
        assertThat(JSONImporter.PAGES, hasItem(isA(TextPageController.class)));
        assertThat(JSONImporter.PREV, is(NOT_FOUND));
        assertThat(JSONImporter.TAGESEINNAHMEN, is(NOT_FOUND));
        assertThat(JSONImporter.UNTERSCHRIFT, is(NOT_FOUND));
        assertThat(JSONImporter.VOR_NR, is(NOT_FOUND));
        assertThat(JSONImporter.WINDOW_TITLE, is(NOT_FOUND));
        assertThat(JSONImporter.ERRORS, notNullValue());
        assertThat(JSONImporter.ERRORS.size(), is(1));
        assertThat(JSONImporter.ERRORS, hasItem(endsWith("xxx.json' existiert nicht.")));
    }

    @Test
    public void loadEmptyFile() {
        JSONImporter.load(EMPTY_FILE_PATH);

        assertThat(JSONImporter.AUSGABEN, is(NOT_FOUND));
        assertThat(JSONImporter.BETRAEGE, is(NOT_FOUND));
        assertThat(JSONImporter.FONT, isA(Font.class));
        assertThat(JSONImporter.FONT, notNullValue());
        assertThat(JSONImporter.LAST_STAND, is(NOT_FOUND));
        assertThat(JSONImporter.NEXT, is(NOT_FOUND));
        assertThat(JSONImporter.PAGES, notNullValue());
        assertThat(JSONImporter.PAGES.size(), is(1));
        assertThat(JSONImporter.PAGES, hasItem(isA(TextPageController.class)));
        assertThat(JSONImporter.PREV, is(NOT_FOUND));
        assertThat(JSONImporter.TAGESEINNAHMEN, is(NOT_FOUND));
        assertThat(JSONImporter.UNTERSCHRIFT, is(NOT_FOUND));
        assertThat(JSONImporter.VOR_NR, is(NOT_FOUND));
        assertThat(JSONImporter.WINDOW_TITLE, is(NOT_FOUND));
        assertThat(JSONImporter.ERRORS, notNullValue());
        assertThat(JSONImporter.ERRORS.size(), is(1));
        assertThat(JSONImporter.ERRORS, hasItem(endsWith("empty_file.json' gefunden.")));
    }

    @Test
    public void loadEmptyTexte() {
        JSONImporter.load(EMPTY_TEXTE_PATH);

        assertThat(JSONImporter.AUSGABEN, is(NOT_FOUND));
        assertThat(JSONImporter.BETRAEGE, is(NOT_FOUND));
        assertThat(JSONImporter.FONT, isA(Font.class));
        assertThat(JSONImporter.FONT, notNullValue());
        assertThat(JSONImporter.LAST_STAND, is(NOT_FOUND));
        assertThat(JSONImporter.NEXT, is(NOT_FOUND));
        assertThat(JSONImporter.PAGES, notNullValue());
        assertThat(JSONImporter.PAGES.size(), is(1));
        assertThat(JSONImporter.PAGES, hasItem(isA(TextPageController.class)));
        assertThat(JSONImporter.PREV, is(NOT_FOUND));
        assertThat(JSONImporter.TAGESEINNAHMEN, is(NOT_FOUND));
        assertThat(JSONImporter.UNTERSCHRIFT, is(NOT_FOUND));
        assertThat(JSONImporter.VOR_NR, is(NOT_FOUND));
        assertThat(JSONImporter.WINDOW_TITLE, is(NOT_FOUND));
        assertThat(JSONImporter.ERRORS, notNullValue());
        assertThat(JSONImporter.ERRORS.size(), is(10));
        assertThat(JSONImporter.ERRORS, everyItem(endsWith("konnte nicht gefunden werden.")));
    }

    @Test
    public void loadEmptyContent() {
        JSONImporter.load(EMPTY_CONTENT_PATH);

        assertThat(JSONImporter.AUSGABEN, is("null"));
        assertThat(JSONImporter.BETRAEGE, is("null"));
        assertThat(JSONImporter.FONT, isA(Font.class));
        assertThat(JSONImporter.FONT, notNullValue());
        assertThat(JSONImporter.LAST_STAND, is("null"));
        assertThat(JSONImporter.NEXT, is("null"));
        assertThat(JSONImporter.PAGES, notNullValue());
        assertThat(JSONImporter.PAGES.size(), is(1));
        assertThat(JSONImporter.PAGES, hasItem(isA(TextPageController.class)));
        assertThat(JSONImporter.PREV, is("null"));
        assertThat(JSONImporter.TAGESEINNAHMEN, is("null"));
        assertThat(JSONImporter.UNTERSCHRIFT, is("null"));
        assertThat(JSONImporter.VOR_NR, is("null"));
        assertThat(JSONImporter.WINDOW_TITLE, is("null"));
        assertThat(JSONImporter.ERRORS, notNullValue());
        assertThat(JSONImporter.ERRORS.size(), is(1));
        assertThat(JSONImporter.ERRORS, everyItem(is("Eingabedatei enthält keine Seiten ('pages')")));
    }

    @Test
    public void loadContent() {
        JSONImporter.load(CONTENT_PATH);

        assertThat(JSONImporter.AUSGABEN, is("PPPPPPPP"));
        assertThat(JSONImporter.BETRAEGE, is("QQQQQQQQ"));
        assertThat(JSONImporter.FONT, isA(Font.class));
        assertThat(JSONImporter.FONT, notNullValue());
        assertThat(JSONImporter.LAST_STAND, is("SSSSSSSS"));
        assertThat(JSONImporter.NEXT, is("NNNNNNNN"));
        assertThat(JSONImporter.PAGES, notNullValue());
        assertThat(JSONImporter.PAGES.size(), is(7));
        assertThat(JSONImporter.PAGES, hasItem(isA(TextPageController.class)));
        assertThat(JSONImporter.PAGES, hasItem(isA(InputLastPageController.class)));
        assertThat(JSONImporter.PAGES, hasItem(isA(InputAdditionalPageController.class)));
        assertThat(JSONImporter.PAGES, hasItem(isA(InputScheinePageController.class)));
        assertThat(JSONImporter.PAGES, hasItem(isA(InputCoinsPageController.class)));
        assertThat(JSONImporter.PAGES, hasItem(isA(ResultPageController.class)));
        assertThat(JSONImporter.PAGES, hasItem(isA(InputCheckPageController.class)));
        assertThat(JSONImporter.PREV, is("OOOOOOOO"));
        assertThat(JSONImporter.TAGESEINNAHMEN, is("TTTTTTTT"));
        assertThat(JSONImporter.UNTERSCHRIFT, is("UUUUUUUU"));
        assertThat(JSONImporter.VOR_NR, is("RRRRRRRR"));
        assertThat(JSONImporter.WINDOW_TITLE, is("MMMMMMMM"));
        assertThat(JSONImporter.ERRORS, notNullValue());
        assertThat(JSONImporter.ERRORS.size(), is(0));
    }
}